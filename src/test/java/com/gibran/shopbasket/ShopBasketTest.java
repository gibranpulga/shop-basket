package com.gibran.shopbasket;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Test;

import com.gibran.shopbasket.item.Item;
import com.gibran.shopbasket.item.ItemFactory;
import com.gibran.shopbasket.item.ItemType;
import com.gibran.shopbasket.itemdiscount.BundleItemDiscountOffer;
import com.gibran.shopbasket.itemdiscount.ItemDiscountOffer;

public class ShopBasketTest {

	final Item apple = ItemFactory.createItem(ItemType.APPLE);
	final Item orange = ItemFactory.createItem(ItemType.ORANGE);
	final Item banana = ItemFactory.createItem(ItemType.BANANA);
	final Item papaya = ItemFactory.createItem(ItemType.PAPAYA);
	ShopBasket shopBasket = new ShopBasket();
	
	public ShopBasketTest() {
		ItemDiscountOffer papayaBundle3for2Offer = new BundleItemDiscountOffer(papaya, 3, 2);
		shopBasket.addOffer(papayaBundle3for2Offer);
	}

	@Test
	public void givenShopBasket_whenCreateShopBasket_shouldBeEmpty() {
		ShopBasket shopBasket = new ShopBasket();
		assertTrue(shopBasket.getSize() == 0);
	}

	@Test
	public void givenShopBasket_whenAddOneItem_shouldReturnOneItem() {
		shopBasket.addItem(apple, 1);
		assertTrue(shopBasket.getSize() == 1);
		assertTrue(shopBasket.getItems().keySet().contains(apple));
	}

	@Test
	public void givenShopBasket_whenAddOneItem_shouldReturnCorrectPrice() {
		shopBasket.addItem(apple, 1);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("0.25")));
	}

	@Test
	public void givenShopBasket_whenAddTwoItems_shouldReturnCorrectPrice() {
		shopBasket.addItem(apple, 1);
		shopBasket.addItem(orange, 1);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("0.55")));
	}

	@Test
	public void givenShopBasket_whenAddTwoSimilarItemsNotInARow_shouldReturnCorrectItemQuantityAndPrice() {
		shopBasket.addItem(apple, 1);
		shopBasket.addItem(orange, 1);
		shopBasket.addItem(apple, 1);
		assertTrue(shopBasket.getSize() == 3);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("0.80")));

	}

	@Test
	public void givenShopBasket_whenAdd3PromotionItemShouldPayFor2_shouldReturnCorrectPrice() {
		shopBasket.addItem(papaya, 3);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("1.00")));
	}

	@Test
	public void givenShopBasket_whenAdd4PromotionItemShouldPayFor3_shouldReturnCorrectPrice() {
		shopBasket.addItem(papaya, 4);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("1.50")));
	}

	@Test
	public void givenShopBasket_whenAdd5PromotionItemShouldPayFor4_shouldReturnCorrectPrice() {
		shopBasket.addItem(papaya, 5);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("2.00")));
	}

	@Test
	public void givenShopBasket_whenAdd6PromotionItemShouldPayFor4_shouldReturnCorrectPrice() {
		shopBasket.addItem(papaya, 6);
		assertTrue(shopBasket.getTotal().equals(new BigDecimal("2.00")));
	}

}
