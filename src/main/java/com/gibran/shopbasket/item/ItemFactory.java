package com.gibran.shopbasket.item;

import java.math.BigDecimal;

public class ItemFactory {

	public static Item createItem(ItemType itemType) throws UnsupportedOperationException {
		switch (itemType) {
		case APPLE:
			return new Item("Apple", new BigDecimal("0.25"));
		case ORANGE:
			return new Item("Orange", new BigDecimal("0.30"));
		case BANANA:
			return new Item("Banana", new BigDecimal("0.15"));
		case PAPAYA:
			return new Item("Papaya", new BigDecimal("0.50"));
		default:
			break;
		}
		throw new UnsupportedOperationException("Item does not exist.");
	}
	
}
