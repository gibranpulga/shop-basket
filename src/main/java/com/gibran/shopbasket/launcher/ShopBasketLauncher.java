package com.gibran.shopbasket.launcher;

import java.util.Scanner;

import com.gibran.shopbasket.ReceiptPrinter;
import com.gibran.shopbasket.ShopBasket;
import com.gibran.shopbasket.item.Item;
import com.gibran.shopbasket.item.ItemFactory;
import com.gibran.shopbasket.item.ItemType;
import com.gibran.shopbasket.itemdiscount.BundleItemDiscountOffer;
import com.gibran.shopbasket.itemdiscount.ItemDiscountOffer;

public class ShopBasketLauncher {
	public static void main(String[] args) {
		ShopBasket shopBasket = new ShopBasket();
		final Item apple = ItemFactory.createItem(ItemType.APPLE);
		final Item orange = ItemFactory.createItem(ItemType.ORANGE);
		final Item banana = ItemFactory.createItem(ItemType.BANANA);
		final Item papaya = ItemFactory.createItem(ItemType.PAPAYA);
		ItemDiscountOffer papayaBundle3for2Offer = new BundleItemDiscountOffer(papaya, 3, 2);
		shopBasket.addOffer(papayaBundle3for2Offer);

		ReceiptPrinter.printWelcome();

		Scanner userInput = new Scanner(System.in);
		String userInputString = "";
		while (!userInputString.equals("exit")) {
			userInputString = userInput.nextLine();
			switch (userInputString) {
			case "exit":
				break;
			case "Apple":
				addItemToShopBasket(shopBasket, apple, userInput);
				break;
			case "Orange":
				addItemToShopBasket(shopBasket, orange, userInput);
				break;
			case "Banana":
				addItemToShopBasket(shopBasket, banana, userInput);
				break;
			case "Papaya":
				addItemToShopBasket(shopBasket, papaya, userInput);
				break;
			case "finish":
				ReceiptPrinter.printRecipt(shopBasket);
				shopBasket = new ShopBasket();
				ReceiptPrinter.printWelcome();
				break;
			case "new":
				shopBasket = new ShopBasket();
				ReceiptPrinter.printWelcome();
				break;
			case "view":
				ReceiptPrinter.printRecipt(shopBasket);
				break;
			default:
				System.out.println("Wrong option, choose again.\n");
				break;
			}
		}
		userInput.close();
	}

	private static void addItemToShopBasket(ShopBasket shopBasket, Item item, Scanner userInput) {
		System.out.println("Quantity? If blank it will be one.\n");
		String userInputString = "";
		Integer quantity = 0;
		do {
			userInputString = userInput.nextLine();
			switch (userInputString) {
			case "":
				quantity = 1;
				break;
			default:
				try {
					quantity = Integer.valueOf(userInputString);
					shopBasket.addItem(item, quantity);
					break;
				} catch (NumberFormatException e) {
					System.out.println("Invalid choice. Type a number!\n");
				}
			}
		} while (!(userInputString.isEmpty() || userInputString.chars().allMatch(character -> Character.isDigit(character))));
		System.out.println(quantity + " " + item.getName() + "(s) added. Next item, or \'finish\' to finalise and print receipt.\n");
		
	}

}
