package com.gibran.shopbasket;

import java.util.Map;

import com.gibran.shopbasket.item.Item;

public class ReceiptPrinter {
	
	// Extract to a class
	public static void printRecipt(ShopBasket shopBasket) {
		System.out.println("===============================================================");
		System.out.println("Here´s the receipt");
		Map<Item, Integer> itemsAndQuantity = shopBasket.getItems();
		StringBuilder recipt = new StringBuilder("Item - Price - Quantity - Item Total \n \n");
		itemsAndQuantity.keySet().stream().forEach(
				item -> recipt.append(item.getName() + " - " + item.getPrice() + " - " + itemsAndQuantity.get(item)
						+ " - " + shopBasket.getItemLineTotal(item, itemsAndQuantity.get(item)) + "\n"));
		recipt.append("\n \n Total: " + shopBasket.getTotal());
		System.out.println(recipt.toString());
		System.out.println("===============================================================");
	}
	
	public static void printWelcome() {
		System.out.println(
				"Choose the items by name. \nItems available:");
		System.out.println("Apple: 0.25");
		System.out.println("Orange: 0.30");
		System.out.println("Banana: 0.15");
		System.out.println("Papaya: 0.50. Promotion: Buy 3, pay for 2.");
		System.out.println("Type \'exit\' to finish, \'view\' to view current basket, \'finish\' to finish the basket, \'new\' to start a new one\n");
	}
}
