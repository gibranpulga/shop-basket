package com.gibran.shopbasket.itemdiscount;

import java.math.BigDecimal;

import com.gibran.shopbasket.item.Item;

public class BundleItemDiscountOffer implements ItemDiscountOffer {

	private Item item;
	private Integer numberOfItemsToBuy;
	private Integer numberOfItemsToPay;

	public BundleItemDiscountOffer(Item item, Integer numberOfItemsToBuy, Integer numberOfItemsToPay) {
		super();
		this.item = item;
		this.numberOfItemsToBuy = numberOfItemsToBuy;
		this.numberOfItemsToPay = numberOfItemsToPay;
	}

	@Override
	public BigDecimal getDiscountedPrice(BigDecimal totalPriceBeforeDiscount, Integer itemQuantityInBasket) {
		BigDecimal itemTotal = BigDecimal.ZERO;
		BigDecimal itemPrice = item.getPrice();

		Integer quantityToCharge;
		if (itemQuantityInBasket >= numberOfItemsToBuy) {
			quantityToCharge = applyDiscount(itemQuantityInBasket);
		} else {
			quantityToCharge = itemQuantityInBasket;
		}

		itemTotal = itemTotal.add(itemPrice.multiply(new BigDecimal(quantityToCharge)));
		return itemTotal;

	}

	private Integer applyDiscount(Integer itemQuantityInBasket) {
		if (itemQuantityInBasket % numberOfItemsToBuy == 0) {
			return applyDiscountForMultipleOfBundleQuantityOffer(itemQuantityInBasket);
		} else {
			return applyDiscountToNotMultipleOfBundleQuantityOffer(itemQuantityInBasket);
		}
	}

	private Integer applyDiscountToNotMultipleOfBundleQuantityOffer(Integer itemQuantityInBasket) {
		return ((itemQuantityInBasket / numberOfItemsToBuy) * numberOfItemsToPay)
				+ (itemQuantityInBasket % numberOfItemsToBuy);
	}

	private Integer applyDiscountForMultipleOfBundleQuantityOffer(Integer itemQuantityInBasket) {
		if (itemQuantityInBasket == numberOfItemsToBuy) {
			return numberOfItemsToPay;
		} else {
			return ((itemQuantityInBasket / numberOfItemsToBuy) * numberOfItemsToPay);
		}
	}

	@Override
	public Item getItem() {
		return item;
	}

}
