package com.gibran.shopbasket.itemdiscount;

import java.math.BigDecimal;

import com.gibran.shopbasket.item.Item;

public class PercentageItemDiscount implements ItemDiscountOffer {
	private Item item;
	private BigDecimal percentageDiscount;

	
	public PercentageItemDiscount(Item item, BigDecimal percentageDiscount) {
		this.item = item;
		this.percentageDiscount = percentageDiscount;
	}

	@Override
	public BigDecimal getDiscountedPrice(BigDecimal priceBeforeDiscount, Integer itemQuantityInBasket) {
		BigDecimal discountAmount = priceBeforeDiscount.divide(percentageDiscount);
		return priceBeforeDiscount.subtract(discountAmount);
	}
	
	@Override
	public Item getItem() {
		return this.item;
	}

}
