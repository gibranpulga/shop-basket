package com.gibran.shopbasket.itemdiscount;

import java.math.BigDecimal;

import com.gibran.shopbasket.item.Item;

public interface ItemDiscountOffer {
	BigDecimal getDiscountedPrice(BigDecimal priceBeforeDiscount, Integer itemQuantityInBasket);
	Item getItem();
}
