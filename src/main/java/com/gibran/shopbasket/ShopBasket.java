package com.gibran.shopbasket;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gibran.shopbasket.item.Item;
import com.gibran.shopbasket.itemdiscount.ItemDiscountOffer;

public class ShopBasket {

	// LinkedHashMap to keep order of items, thinking about facilitating
	private Map<Item, Integer> itemsAndQuantity = new LinkedHashMap<>();
	// LinkedHashSet as a simple way to keep the order of the discounts
	private Set<ItemDiscountOffer> itemDiscounts = new LinkedHashSet<>();

	public BigDecimal getTotal() {
		return itemsAndQuantity
				.keySet()
				.stream()
				.map(item -> getItemLineTotal(item, itemsAndQuantity.get(item)))
				.reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public BigDecimal getItemLineTotal(Item item, Integer itemQuantity) {
		BigDecimal itemPrice = item.getPrice();

		BigDecimal itemTotal = itemPrice.multiply(new BigDecimal(itemQuantity));

		for (ItemDiscountOffer discountOffer : itemDiscounts) {
			itemTotal = applyItemDiscountOffer(item, itemQuantity, itemTotal, discountOffer);
		}

		return itemTotal;
	}

	private BigDecimal applyItemDiscountOffer(Item item, Integer itemQuantity, BigDecimal itemTotal,
			ItemDiscountOffer discountOffer) {
		if (discountOffer.getItem().equals(item)) {
			itemTotal = discountOffer.getDiscountedPrice(itemTotal, itemQuantity);
		}
		return itemTotal;
	}

	public void addOffer(ItemDiscountOffer itemDiscountOffer) {
		this.itemDiscounts.add(itemDiscountOffer);

	}

	public void addItem(Item item, Integer quantity) {
		if (itemsAndQuantity.containsKey(item)) {
			itemsAndQuantity.put(item, itemsAndQuantity.get(item) + quantity);
		} else {
			this.itemsAndQuantity.put(item, quantity);
		}
	}

	public Map<Item, Integer> getItems() {
		return this.itemsAndQuantity;
	}

	public int getSize () {
		return this.getItems()
				.values()
				.stream()
				.map(quantity -> quantity)
				.reduce(0, Integer::sum);
	}



}
