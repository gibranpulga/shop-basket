# shop-basket

I used a TDD approach for the logic behind the application.

The biggest challenge was the offer. First I hardcoded it just to pass the tests. 
Later I changed the approach since I wanted to allow the user to be able to create bundle offers by himself, by selecting the products, 
the amount to buy and the amount to pay (bundle offer). Also, to be able to have more than one offer at the same time. 
I used a very simplistic "rules engine" approach, where multiple offers can be assigned to the shopping basket, and they will be applied in sucession.
I left the class PercentageItemDiscount, even though it´s not being used, I did a quick test with it to check that it would be applied
on top of the BundleItemDiscount Offer.

As next improvement on discounts, to be able to add a discount on the cart, I would create another interface, something like "CartDiscountOffer", 
to be able to base the discount on total amount or quantity or Loyalty points. 
For the Launcher I haven´t created any tests, as its purpose is just to simulate a simple UI.

Also as improvement I would make the Offer configurable by the user, where he could choose the items that he wants to create an offer, 
and then the parameters. For instance if he wanted to create a "Buy 5 pay 4 Apple" he could insert that by himself. It would not be a big change.
Also, make the list of items not hard coded, but an actual list maintaned by the user.


# Assumptions
 
Regarding req '1 - Items are presented one at a time, in a list, 
identified by name - for example "Apple" or “Banana"'. I was not sure if this meant that when the same item is passed
in a different time , it would be shown again, or just shown once and increased the quantity. To me it makes more sense
to just increase the quantity, this way if there are many items, there is no need to pass one by one, and is easier
to check the quantity of each item. That´s the way I would like my receipt to be like.

Regarding req 'Papayas are 50 ct. each but are available as 'three for the price of two'', it was not clear if this meant
that there would only be packages of 3 papayas, and not unitary ones. But to me it made more sense that each 3 papayas, the
customer would pay for 2, so I developed it this way. This way if he buys one will pay for one; if he buys 2 will pay for 2.
But if he buys 3, he will pay for 2. If he buys 4, will pay for 3. If he buys 5, will pay for 4, and if buys 6 will also pay for 4.


# To Execute

Since it was asked to keep it simple, I just included a mvn goal to exec it straigth to mvn, or it can be executed with just the jar with 
regular java -jar command. Ideally I would make it a Spring boot application to create an auto executable, and also could include
a maven profile to generate a docker image for deploy.

So to execute, there are multiple ways:

a) Run command mvn exec:java from this folder;
b) Run command mvn clean install, then from target folder, execute java -jar shop-basket-0.0.1-SNAPSHOT.jar .
c) Run command java -jar shop-basket-0.0.1-SNAPSHOT.jar from this folder.